import Dashboard from './components/Dashboard.vue'
import viewStatus from './components/module_status/viewStatus.vue'
import addStatus from './components/module_status/addStatus.vue'
import updateStatus from './components/module_status/updateStatus.vue'
import viewPosisi from './components/module_posisi/viewPosisi.vue'
import addPosisi from './components/module_posisi/addPosisi.vue'
import updatePosisi from './components/module_posisi/updatePosisi.vue'
import viewPelamar from './components/module_pelamar/viewPelamar.vue'
import addPelamar from './components/module_pelamar/addPelamar.vue'
import updatePelamar from './components/module_pelamar/updatePelamar.vue'
import viewNilaiPelamar from './components/module_pelamar/viewNilaiPelamar.vue'
import addNilaiPelamar from './components/module_pelamar/addNilaiPelamar.vue'
import updateNilaiPelamar from './components/module_pelamar/updateNilaiPelamar.vue'
import viewPelamarLolos from './components/module_riwayat/viewPelamarLolos.vue'
import viewPelamarGagal from './components/module_riwayat/viewPelamarGagal.vue'
import viewUser from './components/module_user/viewUser.vue'
import addUser from './components/module_user/addUser.vue'
import updateUser from './components/module_user/updateUser.vue'
import Login from './components/module_login/Login.vue'
import changePassword from './components/module_user/changePassword.vue'
import viewProfile from './components/module_user/viewProfile'
import NotFound from './components/404.vue'

import jwt_decode from "jwt-decode";

import Vue from 'vue';
import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';
Vue.use(VueToast);

function guardMyroute(to, from, next)
{

let token_exp;
let token = localStorage.getItem('token');
let decodedToken = jwt_decode(token);
// console.log(decodedToken.user_id)
// console.log("Decoded Token", decodedToken);
let currentDate = new Date();

// JWT exp is in seconds
if (decodedToken.exp * 1000 < currentDate.getTime()) {
    // console.log("Token expired.");
    token_exp = true;
} else {
    // console.log("Valid token");
    token_exp = false;
}

if (token_exp==true) {
    console.log('Token Expired!')
    Vue.$toast.open({
        message: 'Token expired, please login again!',
        type: 'error',
        // all of other options may go here
        position: 'top-right'
    });
}

 var isAuthenticated= false;
//this is just an example. You will have to find a better or 
// centralised way to handle you localstorage data handling 
let accessToken = localStorage.getItem('token');
if(accessToken && accessToken !== '' && token_exp == false)
  isAuthenticated = true;
 else
  isAuthenticated= false;
 if(isAuthenticated) 
 {
  next(); // allow to enter route
 } 
 else
 {
  next('/login'); // go to '/login';
 }
}

const routes = [
    {
        name: '/',
        path: '/',
        component: Login
    },
    {
        name: 'NotFound',
        path: '*',
        component: NotFound
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'viewProfile',
        path: '/profile',
        beforeEnter : guardMyroute,
        component: viewProfile
    },
    {
        name: 'changePassword',
        path: '/password',
        beforeEnter : guardMyroute,
        component: changePassword
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        beforeEnter : guardMyroute,
        component: Dashboard
    },
    {
        name: 'viewStatus',
        path: '/status/view',
        beforeEnter : guardMyroute,
        component: viewStatus
    },
    {
        name: 'addStatus',
        path: '/status/add',
        beforeEnter : guardMyroute,
        component: addStatus
    },
    {
        name: 'updateStatus',
        path: '/status/update/:id',
        beforeEnter : guardMyroute,
        component: updateStatus
    },
    {
        name: 'viewPosisi',
        path: '/posisi/view',
        beforeEnter : guardMyroute,
        component: viewPosisi
    },
    {
        name: 'addPosisi',
        path: '/posisi/add',
        beforeEnter : guardMyroute,
        component: addPosisi
    },
    {
        name: 'updatePosisi',
        path: '/posisi/update/:id',
        beforeEnter : guardMyroute,
        component: updatePosisi
    },
    {
        name: 'viewPelamar',
        path: '/pelamar/view',
        beforeEnter : guardMyroute,
        component: viewPelamar
    },
    {
        name: 'addPelamar',
        path: '/pelamar/add',
        beforeEnter : guardMyroute,
        component: addPelamar
    },
    {
        name: 'updatePelamar',
        path: '/pelamar/update/:id',
        beforeEnter : guardMyroute,
        component: updatePelamar
    },
    {
        name: 'viewNilaiPelamar',
        path: '/nilai/view/:id',
        beforeEnter : guardMyroute,
        component: viewNilaiPelamar
    },
    {
        name: 'addNilaiPelamar',
        path: '/nilai/add/:id',
        beforeEnter : guardMyroute,
        component: addNilaiPelamar
    },
    {
        name: 'updateNilaiPelamar',
        path: '/nilai/update/:id',
        beforeEnter : guardMyroute,
        component: updateNilaiPelamar
    },
    {
        name: 'viewPelamarLolos',
        path: '/riwayatlolos/view',
        beforeEnter : guardMyroute,
        component: viewPelamarLolos
    },
    {
        name: 'viewPelamarGagal',
        path: '/riwayatgagal/view',
        beforeEnter : guardMyroute,
        component: viewPelamarGagal
    },
    {
        name: 'viewUser',
        path: '/user/view',
        beforeEnter : guardMyroute,
        component: viewUser
    },
    {
        name: 'addUser',
        path: '/user/add',
        beforeEnter : guardMyroute,
        component: addUser
    },
    {
        name: 'updateUser',
        path: '/user/update/:id',
        beforeEnter : guardMyroute,
        component: updateUser
    },
];
 
export default routes
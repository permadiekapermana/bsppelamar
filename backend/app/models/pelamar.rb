class Pelamar < ApplicationRecord
    # fungsi upload file cv data pelamar
    mount_uploader :cv, CvUploader # Tells rails to use this uploader for this model.
    # validasi data
    validates :nama_pelamar, presence: true
    validates :no_hp, presence: true
    validates :email, presence: true
    validates :asal_instansi, presence: true
    validates :jurusan, presence: true
    validates :grade, presence: true
    validates :id_posisi, presence: true
    validates :id_status, presence: true
end

class Hasiltest < ApplicationRecord
    # fungsi upload file test
    mount_uploader :file_test, TestUploader # Tells rails to use this uploader for this model.
    # validasi data
    validates :nama_test, presence: true
    validates :nilai, presence: true
    validates :id_pelamar, presence: true
end

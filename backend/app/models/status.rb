class Status < ApplicationRecord

    # validate field status tidak boleh kosong, panjang field minimum 3 char
    validates :nama_status, presence: true, length: {minimum: 3}

end

class Posisi < ApplicationRecord

    # validate field posisi tidak boleh kosong, panjang field minimum 3 char
    validates :nama_posisi, presence: true, length: {minimum: 3}

end

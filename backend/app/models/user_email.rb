require 'mail'

class UserEmail < MailForm::Base
    # before_deliver :add_inline_attachment!

    attribute :nama, validate: true
    attribute :email, validate: true
    attribute :email
    validates_format_of :email, with: /\A[^@\s]+@[^@\s]+\z/i
    attribute :nik, validate: true
    attribute :message
    # attribute :file, attachment: true
    # attachments['Doc1.docx'] = File.read('#{Rails.root}/public/uploads/pelamar/cv/4/Doc1.docx')
    # attribute :attachment, attachment: true, validate: true
    def headers       
        {
        #this is the subject for the email generated, it can be anything you want
        subject: "[BSP Data Pelamar] - Registered User",
        to: email,
        from: %("do.not.reply" <#{email}>),
        #the from will display the name entered by the user followed by the email
        template_name: "signup_form",
        # body: File.read(Rails.root + 'public/uploads/pelamar/cv/4/Doc1.doc')
        # attachment['Doc1.docx'] = File.read('D://Doc1.docx')
        
        }
        # attachments: attachments["Doc1.doc"] = File.read(Rails.root + 'public/uploads/pelamar/cv/4/Doc1.doc')
    end

    # private

    # def add_inline_attachment!
    #   attachments["Doc1.doc"] = File.read(Rails.root + 'public/uploads/pelamar/cv/4/Doc1.doc')
    # end

end
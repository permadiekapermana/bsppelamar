class PelamarController < ApplicationController

  def count_apply
    pelamar = Pelamar.select('COUNT(created_at) AS total_apply').where("MONTH(created_at) = ?", DateTime.now.month)
    render json: {
      values: pelamar,
      message: "Success show Count Data!",
    }, pelamar: 200
  end

  def count_progress
    pelamar = Pelamar.select('COUNT(pelamars.created_at) AS total_progress').joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("statuses.jenis_status = ? AND MONTH(pelamars.created_at) = ?", 'Progress', DateTime.now.month)
    render json: {
      values: pelamar,
      message: "Success show Count Data!",
    }, pelamar: 200
  end

  def count_lolos
    pelamar = Pelamar.select('COUNT(pelamars.created_at) AS total_lolos').joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("statuses.jenis_status = ? AND MONTH(pelamars.created_at) = ?", 'Lolos', DateTime.now.month)
    render json: {
      values: pelamar,
      message: "Success show Count Data!",
    }, pelamar: 200
  end

  def count_gagal
    pelamar = Pelamar.select('COUNT(pelamars.created_at) AS total_gagal').joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("statuses.jenis_status = ? AND MONTH(pelamars.created_at) = ?", 'Gagal', DateTime.now.month)
    render json: {
      values: pelamar,
      message: "Success show Count Data!",
    }, pelamar: 200
  end

  # index / show all data
  def index
    pelamar = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").order(id: :desc)
    render json: {
      values: pelamar,
      message: "Success show All Data!",
    }, pelamar: 200
  end

  def progress
    $nama_pelamar = params[:nama_pelamar]
    $id_posisi = params[:id_posisi]
    $id_status = params[:id_status]
    
    if params[:nama_pelamar] && params[:id_posisi] && params[:id_status]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, pelamars.id_status, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_posisi = ? AND pelamars.id_status = ?  AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_posisi,  $id_status, 'Progress').order(id: :desc) 
    elsif params[:nama_pelamar] && params[:id_posisi]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_posisi = ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_posisi, 'Progress').order(id: :desc)
    elsif params[:nama_pelamar] && params[:id_status]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_status = ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_status, 'Progress').order(id: :desc)
    elsif params[:id_status] && params[:id_posisi]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_status = ? AND pelamars.id_posisi = ? AND statuses.jenis_status = ?", $id_status, $id_posisi, 'Progress').order(id: :desc)
    elsif params[:nama_pelamar]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", 'Progress').order(id: :desc)
    elsif params[:id_posisi]
        $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_posisi = ? AND statuses.jenis_status = ?", $id_posisi, 'Progress').order(id: :desc)
    elsif params[:id_status]
        $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_status = ? AND statuses.jenis_status = ?", $id_status, 'Progress').order(id: :desc)
    else
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("statuses.jenis_status = ?", 'Progress').order(id: :desc)
    end

    pelamar = $select
    render json: {
      values: pelamar,
      message: "Success show All Data!",
    }, pelamar: 200
  end

  def lolos
    $nama_pelamar = params[:nama_pelamar]
    $id_posisi = params[:id_posisi]
    $id_status = params[:id_status]

    if params[:nama_pelamar] && params[:id_posisi] && params[:id_status]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, pelamars.id_status, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_posisi = ? AND pelamars.id_status = ?  AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_posisi,  $id_status, 'Lolos').order(id: :desc)    
    elsif params[:nama_pelamar] && params[:id_posisi]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_posisi = ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_posisi, 'Lolos').order(id: :desc)    
    elsif params[:nama_pelamar] && params[:id_status]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_status = ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_status, 'Lolos').order(id: :desc)
    elsif params[:id_status] && params[:id_posisi]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_status = ? AND pelamars.id_posisi = ? AND statuses.jenis_status = ?", $id_status, $id_posisi, 'Lolos').order(id: :desc)
    elsif params[:nama_pelamar]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", 'Lolos').order(id: :desc)
    elsif params[:id_posisi]
        $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_posisi = ? AND statuses.jenis_status = ?", $id_posisi, 'Lolos').order(id: :desc)
    elsif params[:id_status]
        $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_status = ? AND statuses.jenis_status = ?", $id_status, 'Lolos').order(id: :desc)
    else
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("statuses.jenis_status = ?", 'Lolos').order(id: :desc)
    end

    pelamar = $select
    render json: {
      values: pelamar,
      message: "Success show All Data!",
    }, pelamar: 200
  end

  def gagal
    $nama_pelamar = params[:nama_pelamar]
    $id_posisi = params[:id_posisi]
    $id_status = params[:id_status]
    
    if params[:nama_pelamar] && params[:id_posisi] && params[:id_status]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, pelamars.id_status, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_posisi = ? AND pelamars.id_status = ?  AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_posisi,  $id_status, 'Gagal').order(id: :desc)   
    elsif params[:nama_pelamar] && params[:id_posisi]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_posisi = ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_posisi, 'Gagal').order(id: :desc)
    elsif params[:nama_pelamar] && params[:id_status]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND pelamars.id_status = ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", $id_status, 'Gagal').order(id: :desc)
    elsif params[:id_status] && params[:id_posisi]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_status = ? AND pelamars.id_posisi = ? AND statuses.jenis_status = ?", $id_status, $id_posisi, 'Gagal').order(id: :desc)
    elsif params[:nama_pelamar]
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.nama_pelamar LIKE ? AND statuses.jenis_status = ?", "%#{$nama_pelamar}%", 'Gagal').order(id: :desc)
    elsif params[:id_posisi]
        $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_posisi = ? AND statuses.jenis_status = ?", $id_posisi, 'Gagal').order(id: :desc)
    elsif params[:id_status]
        $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("pelamars.id_status = ? AND statuses.jenis_status = ?", $id_status, 'Gagal').order(id: :desc)
    else
      $select = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").where("statuses.jenis_status = ?", 'Gagal').order(id: :desc)
    end

    pelamar = $select
    render json: {
      values: pelamar,
      message: "Success show All Data!",
    }, pelamar: 200
  end

  # show data by ID
  def show
    pelamar = Pelamar.select('pelamars.id, pelamars.nama_pelamar, pelamars.no_hp, pelamars.email, pelamars.asal_instansi, pelamars.jurusan, pelamars.grade, pelamars.cv, posisis.nama_posisi, statuses.nama_status, pelamars.id_status, pelamars.id_posisi, pelamars.created_at, pelamars.updated_at').joins("INNER JOIN posisis ON pelamars.id_posisi = posisis.id").joins("INNER JOIN statuses ON pelamars.id_status = statuses.id").order(id: :desc).find_by_id(params[:id])
    if pelamar.present?
      render json: {
        values: pelamar,
        message: "Success show Data!",
      }, pelamar: 200
    else
      render json: {
        values: "",
        message: "We can't found any Data!",
      }, pelamar: 400
    end
  end

  # create / post
  def create

    pelamar  = Pelamar.new(pelamar_params)
    if pelamar.save
      # pelamar created = 201
      render json: {
        values: pelamar,
        message: "Success Insert Data!",
      }, pelamar: :created      
    else
      # pelamar unprocessable entity = 422
      render json: {
        values: pelamar.errors,
        message: "Failed Insert Data!",
      }, pelamar: :unprocessable_entity
    end

  end

  # update / put
  def update
    pelamar = Pelamar.find(params[:id])
    if pelamar.update(pelamar_params)
      # pelamar created = 201
      render json: {
        values: pelamar,
        message: "Success Update Data!",
      }, pelamar: :created      
    else
      # pelamar unprocessable entity = 422
      render json: {
        values: pelamar.errors,
        message: "Failed Update Data!",
      }, pelamar: :unprocessable_entity
    end

  end

  # delete / destroy pelamar
  def destroy
    pelamar = Pelamar.find(params[:id]).destroy!
      if pelamar.destroy
        # pelamar created = 201
        render json: {
          values: {},
          message: "Success Delete Data!",
        }, pelamar: :created      
      else
        # pelamar unprocessable entity = 422
        render json: {
          values: pelamar.errors,
          message: "Failed Delete Data!",
        }, pelamar: :unprocessable_entity
      end
  end

  # private class
  private
  def pelamar_params
    params.permit(:nama_pelamar, :no_hp, :email, :asal_instansi, :jurusan, :grade, :cv, :id_posisi, :id_status, :nama_posisi)
  end

  # function not destroyed
  def not_destroyed
  end

end

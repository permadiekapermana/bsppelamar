class PosisiController < ApplicationController

  # index / show all data
  def index
    posisi = Posisi.all
    render json: {
      values: posisi,
      message: "Success show All Data!",
    }, status: 200
  end

  # show data by ID
  def show
    posisi = Posisi.find_by_id(params[:id])
    if posisi.present?
      render json: {
        values: posisi,
        message: "Success show Data!",
      }, status: 200
    else
      render json: {
        values: "",
        message: "We can't found any Data!",
      }, status: 400
    end
  end

  # create / post
  def create

    posisi  = Posisi.new(posisi_params)
    if posisi.save
      # status created = 201
      render json: {
        values: posisi,
        message: "Success Insert Data!",
      }, status: :created      
    else
      # status unprocessable entity = 422
      render json: {
        values: posisi.errors,
        message: "Failed Insert Data!",
      }, status: :unprocessable_entity
    end

  end

  # update / put
  def update
    posisi = Posisi.find(params[:id])
    if posisi.update(posisi_params)
      # status created = 201
      render json: {
        values: posisi,
        message: "Success Update Data!",
      }, status: :created      
    else
      # status unprocessable entity = 422
      render json: {
        values: posisi.errors,
        message: "Failed Update Data!",
      }, status: :unprocessable_entity
    end

  end

  # delete / destroy nama posisi
  def destroy
    posisi = Posisi.find(params[:id]).destroy!
      if posisi.destroy
        # status created = 201
        render json: {
          values: {},
          message: "Success Delete Data!",
        }, status: :created      
      else
        # status unprocessable entity = 422
        render json: {
          values: posisi.errors,
          message: "Failed Delete Data!",
        }, status: :unprocessable_entity
      end
  end

  # private class
  private
  def posisi_params
    params.permit(:nama_posisi)
  end

  # function not destroyed
  def not_destroyed
  end
  
end

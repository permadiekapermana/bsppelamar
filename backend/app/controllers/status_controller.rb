class StatusController < ApplicationController

  # index / show all data
  def index
    status = Status.all
    render json: {
      values: status,
      message: "Success show All Data!",
    }, status: 200
  end

  def progress
    status = Status.select('statuses.id, statuses.jenis_status, statuses.nama_status').where("statuses.jenis_status = ?", "Progress").order(id: :asc)
    render json: {
      values: status,
      message: "Success show All Data!",
    }, status: 200
  end

  def lolos
    status = Status.select('statuses.id, statuses.jenis_status, statuses.nama_status').where("statuses.jenis_status = ?", "Lolos").order(id: :asc)
    render json: {
      values: status,
      message: "Success show All Data!",
    }, status: 200
  end

  def gagal
    status = Status.select('statuses.id, statuses.jenis_status, statuses.nama_status').where("statuses.jenis_status = ?", "Gagal").order(id: :asc)
    render json: {
      values: status,
      message: "Success show All Data!",
    }, status: 200
  end

  # show data by ID
  def show
    status = Status.find_by_id(params[:id])
    if status.present?
      render json: {
        values: status,
        message: "Success show Data!",
      }, status: 200
    else
      render json: {
        values: "",
        message: "We can't found any Data!",
      }, status: 400
    end
  end

  # create / post
  def create

    status  = Status.new(status_params)
    if status.save
      # status created = 201
      render json: {
        values: status,
        message: "Success Insert Data!",
      }, status: :created      
    else
      # status unprocessable entity = 422
      render json: {
        values: status.errors,
        message: "Failed Insert Data!",
      }, status: :unprocessable_entity
    end

  end

  # update / put
  def update
    status = Status.find(params[:id])
    if status.update(status_params)
      # status created = 201
      render json: {
        values: status,
        message: "Success Update Data!",
      }, status: :created      
    else
      # status unprocessable entity = 422
      render json: {
        values: status.errors,
        message: "Failed Update Data!",
      }, status: :unprocessable_entity
    end

  end

  # delete / destroy status
  def destroy
    status = Status.find(params[:id]).destroy!
      if status.destroy
        # status created = 201
        render json: {
          values: {},
          message: "Success Delete Data!",
        }, status: :created      
      else
        # status unprocessable entity = 422
        render json: {
          values: status.errors,
          message: "Failed Delete Data!",
        }, status: :unprocessable_entity
      end
  end

  # private class
  private
  def status_params
    params.permit(:jenis_status, :nama_status)
  end

  # function not destroyed
  def not_destroyed
  end
  
end

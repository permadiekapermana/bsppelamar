class UsersController < ApplicationController

    before_action :authorized, only: [:auto_login]

    # index / show all data
    def index
        user = User.all
        render json: {
        values: user,
        message: "Success show All Data!",
        }, status: 200
    end

    # show data by ID
    def show
        user = User.find_by_id(params[:id])
        if user.present?
        render json: {
            values: user,
            message: "Success show Data!",
        }, status: 200
        else
        render json: {
            values: "",
            message: "We can't found any Data!",
        }, status: 400
        end
    end

    # REGISTER
    def create

        @user_new = User.create(user_params)
        if @user_new.valid?
            token = encode_token({user_id: @user_new.id})
            @user = UserEmail.new(email_params)
            # @user.name = params[:name]
            # @user.nik = params[:nik]
            # @user.email = params[:email]
            # @user.message = params[:message]
            @user.message = 'Akun anda telah didaftarkan pada sistem pengolahan data pelamar BSP, silahkan login dengan menggunakan NIK anda sebagai username dan password. Harap segera ubah password ketika sudah berhasil login!'
            # @user.file = '#{Rails.root}/public/uploads/pelamar/cv/4/Doc1.docx'
            # attachment = SpecialEncode(File.read(Rails.root + 'public/uploads/pelamar/cv/4/Doc1.doc'))
            # attachments['Doc1.doc'] = {
            # mime_type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            # encoding: 'SpecialEncoding',
            # content: encoded_content
            # }
            if @user.deliver
            render json: {user: @user_new, token: token, message: "Email sent successfully"}
            else
            render json: {user: @user_new, token: token, message: "Email not sent"}
            end
        else
            render json: {error: "Invalid username or password"}
        end

        # @user = UserEmail.new()
        # @user.name = params[:name]
        # @user.nik = params[:nik]
        # @user.email = params[:email]
        # # @user.message = params[:message]
        # @user.message = 'Akun anda telah didaftarkan pada sistem pengolahan data pelamar BSP, silahkan login dengan menggunakan NIK anda sebagai username dan password. Harap segera ubah password ketika sudah berhasil login!'
        # if @user.deliver
        # render json: {message: "Email sent successfully"}
        # else
        # render json: @user.errors
        # end

        # @user = User.create(user_params)
        # if @user.valid?
        # token = encode_token({user_id: @user.id})
        # render json: {user: @user, token: token}
        # else
        # render json: {error: "Invalid username or password"}
        # end
    end

    # delete / destroy user
    def destroy
        user = User.find(params[:id]).destroy!
        if user.destroy
            # user created = 201
            render json: {
            values: {},
            message: "Success Delete Data!",
            }, status: :created      
        else
            # user unprocessable entity = 422
            render json: {
            values: user.errors,
            message: "Failed Delete Data!",
            }, status: :unprocessable_entity
        end
    end

    # update / put
    def update
        user = User.find(params[:id])
        if user.update(user_params)
        # user created = 201
        render json: {
            values: user,
            message: "Success Update Data!",
        }, status: :created      
        else
        # user unprocessable entity = 422
        render json: {
            values: user.errors,
            message: "Failed Update Data!",
        }, status: :unprocessable_entity
        end

    end

    # LOGGING IN
    def login
        @user = User.find_by(username: params[:username])

        if @user && @user.authenticate(params[:password])
        token = encode_token({user_id: @user.id})
        render json: {user: @user, token: token}
        else
        render json: {error: "Invalid username or password"}
        end
    end


    def auto_login
        render json: @user
    end

    private

    def user_params
        params.permit(:username, :password, :nama)
    end

    def email_params
        params.permit(:nama, :email, :nik)
    end
    
end

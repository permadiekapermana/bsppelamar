class HasiltestController < ApplicationController

  # index / show all data
  def index
    hasiltest = Hasiltest.where("id_pelamar = ?", params[:id])
    render json: {
      values: hasiltest,
      message: "Success show All Data!",
    }, hasiltest: 200
  end

  # show data by ID
  def show
    hasiltest = Hasiltest.find_by_id(params[:id])
    if hasiltest.present?
      render json: {
        values: hasiltest,
        message: "Success show Data!",
      }, hasiltest: 200
    else
      render json: {
        values: "",
        message: "We can't found any Data!",
      }, hasiltest: 400
    end
  end

  # create / post
  def create

    hasiltest  = Hasiltest.new(hasiltest_params)
    if hasiltest.save
      # hasiltest created = 201
      render json: {
        values: hasiltest,
        message: "Success Insert Data!",
      }, hasiltest: :created      
    else
      # hasiltest unprocessable entity = 422
      render json: {
        values: hasiltest.errors,
        message: "Failed Insert Data!",
      }, hasiltest: :unprocessable_entity
    end

  end

  # update / put
  def update
    hasiltest = Hasiltest.find(params[:id])
    if hasiltest.update(hasiltest_params)
      # hasiltest created = 201
      render json: {
        values: hasiltest,
        message: "Success Update Data!",
      }, hasiltest: :created      
    else
      # hasiltest unprocessable entity = 422
      render json: {
        values: hasiltest.errors,
        message: "Failed Update Data!",
      }, hasiltest: :unprocessable_entity
    end

  end

  # delete / destroy hasiltest
  def destroy
    hasiltest = Hasiltest.find(params[:id]).destroy!
      if hasiltest.destroy
        # hasiltest created = 201
        render json: {
          values: {},
          message: "Success Delete Data!",
        }, hasiltest: :created      
      else
        # hasiltest unprocessable entity = 422
        render json: {
          values: hasiltest.errors,
          message: "Failed Delete Data!",
        }, hasiltest: :unprocessable_entity
      end
  end

  # private class
  private
  def hasiltest_params
    params.permit(:nama_test, :nilai, :file_test, :id_pelamar)
  end

  # function not destroyed
  def not_destroyed
  end

end

class ApplicationController < ActionController::API

    # exception error handle failed destroy data
    rescue_from ActiveRecord::RecordNotDestroyed, with: :not_destroyed

    before_action :authorized

    def encode_token(payload, exp = 24.hours.from_now)
    # def encode_token(payload, exp = 1.minutes.from_now)
        payload[:exp] = exp.to_i
        JWT.encode(payload, 'SuperSuperSecretKey')
    end

    def auth_header
        # { Authorization: 'Bearer <token>' }
        request.headers['Authorization']
    end

    def decoded_token
        if auth_header
        token = auth_header.split(' ')[1]
        # header: { 'Authorization': 'Bearer <token>' }
        begin
            JWT.decode(token, 'SuperSuperSecretKey', true, algorithm: 'HS256')
        rescue JWT::DecodeError
            nil
        end
        end
    end

    def logged_in_user
        if decoded_token
        user_id = decoded_token[0]['user_id']
        @user = User.find_by(id: user_id)
        end
    end

    def logged_in?
        !!logged_in_user
    end

    def authorized
        render json: { message: 'Please log in' }, status: :unauthorized unless logged_in?
    end

    private
    #function not destroyed
    def not_destroyed(e)
        render json: {errors: e.record.errors}, status: :unprocessable_entity
    end
end

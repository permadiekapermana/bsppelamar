require 'rails_helper'
# migrate DB
# hapus db test
# migrate db:migrate
# bin/rails db:migrate RAILS_ENV=test
# rspec --format=documentation

describe 'Posisi API', type: :request do

    describe 'GET /posisi' do
        it 'Return All Data Posisi' do
            get '/posisi'
            expect(response).to have_http_status(:success)
            # expect(JSON.parse(response.body.values).size).to eq(2)
        end
    end

    describe 'POST /posisi' do
        it 'Insert Data Posisi' do
            post '/posisi', params: {nama_posisi: 'Programmer'}
            expect(response).to have_http_status(:created)
            # expect(JSON.parse(response.body.values).size).to eq(2)
        end
    end

end
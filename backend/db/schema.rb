# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_17_130755) do

  create_table "hasiltests", charset: "utf8mb4", force: :cascade do |t|
    t.string "nama_test"
    t.integer "nilai"
    t.string "file_test"
    t.bigint "id_pelamar"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id_pelamar"], name: "fk_rails_014874fe9f"
  end

  create_table "pelamars", charset: "utf8mb4", force: :cascade do |t|
    t.string "nama_pelamar"
    t.string "no_hp"
    t.string "email"
    t.string "asal_instansi"
    t.string "jurusan"
    t.string "grade"
    t.string "cv"
    t.bigint "id_posisi"
    t.bigint "id_status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id_posisi"], name: "fk_rails_5b06b79d7c"
    t.index ["id_status"], name: "fk_rails_49269ef74d"
  end

  create_table "posisis", charset: "utf8mb4", force: :cascade do |t|
    t.string "nama_posisi"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "statuses", charset: "utf8mb4", force: :cascade do |t|
    t.string "jenis_status"
    t.string "nama_status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", charset: "utf8mb4", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.string "nama"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "hasiltests", "pelamars", column: "id_pelamar"
  add_foreign_key "pelamars", "posisis", column: "id_posisi"
  add_foreign_key "pelamars", "statuses", column: "id_status"
end

class CreateStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :statuses do |t|
      t.string :jenis_status
      t.string :nama_status
      t.timestamps
    end
  end
end

class CreatePosisis < ActiveRecord::Migration[6.1]
  def change
    create_table :posisis do |t|
      t.string :nama_posisi
      t.timestamps
    end
  end
end

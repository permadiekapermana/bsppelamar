class CreatePelamars < ActiveRecord::Migration[6.1]
  def change
    create_table :pelamars do |t|
      t.string :nama_pelamar
      t.string :no_hp
      t.string :email
      t.string :asal_instansi
      t.string :jurusan
      t.string :grade
      t.string :cv
      t.bigint :id_posisi
      t.bigint :id_status
      t.timestamps
    end
    add_foreign_key :pelamars, :posisis, column: :id_posisi
    add_foreign_key :pelamars, :statuses, column: :id_status
  end
end

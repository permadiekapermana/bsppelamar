class CreateHasiltests < ActiveRecord::Migration[6.1]
  def change
    create_table :hasiltests do |t|
      t.string :nama_test
      t.integer :nilai
      t.string :file_test
      t.bigint :id_pelamar
      t.timestamps
      # t.index :id_pelamar      
    end
    add_foreign_key :hasiltests, :pelamars, column: :id_pelamar
  end
end

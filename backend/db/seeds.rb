# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
posisi = Posisi.create(
    [
        {nama_posisi: 'Programmer'},
        {nama_posisi: 'Quality Assurance (QA)'},
        {nama_posisi: 'Sistem Analis (SA)'}
    ]
)

status = Status.create(
    [
        {
            jenis_status: 'Progress',
            nama_status: 'Apply'
        },
        {
            jenis_status: 'Progress',
            nama_status: 'Terjadwal Test Logic'
        },
        {
            jenis_status: 'Progress',
            nama_status: 'Terjadwal Test Study Case'
        },
        {
            jenis_status: 'Progress',
            nama_status: 'Terjadwal Test Wawancara'
        },
        {
            jenis_status: 'Gagal',
            nama_status: 'Tidak Lulus Test Logic'
        },
        {
            jenis_status: 'Gagal',
            nama_status: 'Tidak Lulus Test Study Case'
        },
        {
            jenis_status: 'Gagal',
            nama_status: 'Tidak Lulus Wawancara'
        },
        {
            jenis_status: 'Lolos',
            nama_status: 'Bootcamp'
        },
        {
            jenis_status: 'Lolos',
            nama_status: 'Diterima'
        }
    ]
)

pelamar = Pelamar.create(
    [
        {
            nama_pelamar: 'Permadi Eka P.',
            no_hp: '08xxxxxxxxxx',
            email: 'permadiekapermana@gmail.com',
            asal_instansi: 'Universitas Muhammadiyah Cirebon',
            jurusan: 'Teknik Informatika',
            grade: 'S1',
            id_posisi: 1,
            id_status: 1
        },
        {
            nama_pelamar: 'Abdul Fattah',
            no_hp: '08xxxxxxxxxx',
            email: 'abdulfattah@gmail.com',
            asal_instansi: 'Universitas Muhammadiyah Cirebon',
            jurusan: 'Teknik Informatika',
            grade: 'S1',
            id_posisi: 1,
            id_status: 1
        }
    ]
)

hasiltest = Hasiltest.create(
    [
        {
            nama_test: 'Test Logic',
            nilai: 80,
            id_pelamar: 1
        },
        {
            nama_test: 'Test Programming',
            nilai: 80,
            id_pelamar: 1
        },
        {
            nama_test: 'Test Logic',
            nilai: 80,
            id_pelamar: 2
        },
        {
            nama_test: 'Test Programming',
            nilai: 80,
            id_pelamar: 2
        }
    ]
)

user = User.create(
    [
        {
            username: "permadi",
            password: "permadi",
            nama: "Permadi Eka Permana"
        },
        {
            username: "endah",
            password: "endah",
            nama: "Endah Febiana Gunawan"
        }
    ]
)
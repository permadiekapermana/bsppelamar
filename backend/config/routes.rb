Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'posisi/index'  
  resources :posisi, only: [:index, :show, :create, :update, :destroy]

  get 'status/index'  
  resources :status, only: [:index, :show, :create, :update, :destroy]

  get '/status_progress' => 'status#progress'
  get '/status_lolos' => 'status#lolos'
  get '/status_gagal' => 'status#gagal'

  get 'pelamar/index'  
  resources :pelamar, only: [:index, :show, :create, :update, :destroy]

  get '/pelamar_progress' => 'pelamar#progress'
  get '/pelamar_lolos' => 'pelamar#lolos'
  get '/pelamar_gagal' => 'pelamar#gagal'
  get '/count_apply' => 'pelamar#count_apply'
  get '/count_progress' => 'pelamar#count_progress'
  get '/count_lolos' => 'pelamar#count_lolos'
  get '/count_gagal' => 'pelamar#count_gagal'

  get 'hasiltest/index'
  resources :hasiltest, only: [:index, :show, :create, :update, :destroy]

  resources :users, only: [:index, :show, :create, :update, :destroy]
  post '/login' => 'users#login'
  get '/auto_login' => 'users#auto_login'
end